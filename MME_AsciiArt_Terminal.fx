// グレースケール化
// 0.YUV 1.明度 2.光度 3.平均 4.HSV   1,2,3はGIMPの"脱色"と同等
#define METHOD_GRAYSCALE 0


//ポストエフェクト
float Script : STANDARDSGLOBAL <
    string ScriptOutput = "color";
    string ScriptClass = "scene";
    string ScriptOrder = "postprocess";
> = 0.8;

// 背景のクリア値
float4 ClearColor = {0,0,0,1};
float ClearDepth  = 1.0;

#define ASCII_WIDTH  8.0f  //横
#define ASCII_HEIGHT 16.0f //縦


#define SAMP_MODE(minf, magf, mipf, addu, addv) \
    MinFilter = minf; MagFilter = magf; MipFilter = mipf; AddressU = addu; AddressV = addv;


// オリジナルの描画結果を記録するためのレンダーターゲット
texture ScnMap : RENDERCOLORTARGET <
    float2 ViewPortRatio = {1.0,1.0};
    int MipLevels = 0;
>;

sampler ScnSamp = sampler_state {
    texture = <ScnMap>;
    SAMP_MODE(LINEAR, LINEAR, LINEAR, CLAMP, CLAMP)
};

// 深度バッファ
texture DepthBuffer : RENDERDEPTHSTENCILTARGET <
    float2 ViewPortRatio = {1.0,1.0};
>;


// グレースケール 1/8サイズ
texture ScnMap_GRAY : RENDERCOLORTARGET <
    float2 ViewPortRatio = {0.125,0.125};
    int MipLevels = 1;
>;

sampler ScnSamp_GRAY = sampler_state {
    texture = <ScnMap_GRAY>;
    SAMP_MODE(POINT, POINT, POINT, CLAMP, CLAMP)
};


// 判定 左右1/8,上下1/16サイズ
texture ScnMap_Judge : RENDERCOLORTARGET <
    float2 ViewPortRatio = {0.125,0.0625};
    int MipLevels = 1;
>;

sampler ScnSamp_Judge = sampler_state {
    texture = <ScnMap_Judge>;
    SAMP_MODE(POINT, POINT, POINT, CLAMP, CLAMP)
};



// 文字テクスチャ
texture2D AsciiTex < string ResourceName = "ascii_tex_Terminal.png"; >;
sampler2D AsciiSamp = sampler_state {
    texture = <AsciiTex>;
    SAMP_MODE(POINT, POINT, POINT, CLAMP, CLAMP)
};



// スクリーンサイズ依存値
float2 ViewportSize : VIEWPORTPIXELSIZE;
static float2 ViewportOffset = (float2(0.5f,0.5f)/ViewportSize);
static float2 ViewportOnePix = ViewportOffset * float2(2.0f,2.0f);
static float2 ViewportAsciiSize = (float2(ASCII_WIDTH,ASCII_HEIGHT)/ViewportSize);


// 文字テクスチャ
static float2 AsciiTexSize = float2(1024.0f,64.0f);
static float2 AsciiTexOffset = (float2(0.5,0.5)/AsciiTexSize);
static float2 AsciiTexOnePix = AsciiTexOffset * float2(2.0f,2.0f);


////////////////////////////////////////////////////////////////////////////////////////////////
//共通
struct VS_OUTPUT {
    float4 Pos          : POSITION;
    float2 Tex          : TEXCOORD0;
};


//一部共通
VS_OUTPUT VS_BasicBuffer( float4 Pos : POSITION, float4 Tex : TEXCOORD0 )
{
    VS_OUTPUT Out; 
    
    Out.Pos = Pos;
    Out.Tex = Tex + ViewportOffset;

    return Out;
}
////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
//グレースケール
VS_OUTPUT VS_Bleach( float4 Pos : POSITION, float4 Tex : TEXCOORD0 )
{
    VS_OUTPUT Out; 
    
    Out.Pos = Pos;
    Out.Tex = Tex + ViewportOffset*float2(0.125f,0.125f);

   return Out;
}

float4 PS_Bleach(float2 Tex: TEXCOORD0) : COLOR
{
    //float4 Color = tex2Dlod(ScnSamp,float4(Tex,0,3 ));
    float4 Color = tex2D(ScnSamp, Tex);

    // 0.YUV 1.明度 2.光度 3.平均 4.HSV
    #if METHOD_GRAYSCALE == 0
        float Y =  0.299*Color.r + 0.587*Color.g + 0.114*Color.b;
        Color = float4(Y,Y,Y,1);

    #elif METHOD_GRAYSCALE == 1
        Color = 0.5 * ( max(max(Color.r,Color.g),Color.b) + min(min(Color.r,Color.g),Color.b) );

    #elif METHOD_GRAYSCALE == 2
        float L =  0.21*Color.r + 0.72*Color.g + 0.07*Color.b;
        Color = float4(L,L,L,1);

    #elif METHOD_GRAYSCALE == 3
        Color = (Color.r + Color.g + Color.b ) / 3.0;

    #elif METHOD_GRAYSCALE == 4
        float V = max(max(Color.r,Color.g),Color.b);
        Color = float4(V,V,V,1);

    #endif


    return Color;
}
////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////
//特徴判断
struct VS_JUDGE_OUTPUT {
    float4 Pos          : POSITION;
    float2 TexUpper     : TEXCOORD0;
    float2 TexBottom    : TEXCOORD1;
    //float2 TexCenter    : TEXCOORD2;
};

VS_JUDGE_OUTPUT VS_Judge( float4 Pos : POSITION, float4 Tex : TEXCOORD0 )
{
    VS_JUDGE_OUTPUT Out; 
    
    Out.Pos = Pos;
    Out.TexUpper  = Tex + ViewportOffset*float2(0.125f,0.0625f-0.03125f);
    Out.TexBottom = Tex + ViewportOffset*float2(0.125f,0.0625f+0.03125f);

   return Out;
}

float4 PS_Judge(VS_JUDGE_OUTPUT IN) : COLOR
{
    float UpperLuminous  = tex2D(ScnSamp_GRAY,IN.TexUpper ).r;
    float BottomLuminous = tex2D(ScnSamp_GRAY,IN.TexBottom).r;
    float AvgLuminous = (UpperLuminous + BottomLuminous)/2.0f;

    //　上0 下0.25 中0.5　　　AsciiTex.pngのYに対応
    float bias;
    if(UpperLuminous*0.85>BottomLuminous){
        bias = 0.0;
    }else if(BottomLuminous*0.85>UpperLuminous){
        bias = 0.25;
    }else{
        bias = 0.5;
    }

    float4 Color = float4(0,0,0,1);
    Color.r = (AvgLuminous==1.0)?254.0f/255.0f:AvgLuminous;
    Color.g = bias;

    return Color;
}

////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
//画面出力
float4 PS_BasicBuffer(float2 Tex: TEXCOORD0) : COLOR
{
    float4 JudgeColor = tex2D(ScnSamp_Judge,Tex); 
    
    float2 AsciiTexUV = float2(floor(JudgeColor.r/(ASCII_WIDTH/1024.0f))*AsciiTexOnePix.x*ASCII_WIDTH + fmod(Tex.x,ViewportAsciiSize.x)/ViewportOnePix.x*AsciiTexOnePix.x,
                               JudgeColor.g + fmod(Tex.y,ViewportAsciiSize.y)/ViewportOnePix.y*AsciiTexOnePix.y )
                       + AsciiTexOffset;
                       
    float4 Color = tex2D(AsciiSamp,AsciiTexUV);

    return Color;
}
/////////////////////////////////////////////////////////////////////////////////



technique PostEffect <
    string Script = 
        "RenderColorTarget0=ScnMap;"
        "RenderDepthStencilTarget=DepthBuffer;"
            "ClearSetColor=ClearColor;"
            "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
        "ScriptExternal=Color;"

        "RenderColorTarget0=ScnMap_GRAY;"
        "RenderDepthStencilTarget=DepthBuffer;"
            "ClearSetColor=ClearColor;"
            "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
        "Pass=Bleach;"

        "RenderColorTarget0=ScnMap_Judge;"
        "RenderDepthStencilTarget=DepthBuffer;"
            "ClearSetColor=ClearColor;"
            "ClearSetDepth=ClearDepth;"
                "Clear=Color;"
                "Clear=Depth;"
        "Pass=Judge;"


        "RenderColorTarget0=;"
        "RenderDepthStencilTarget=;"
        "Pass=DrawBuffer;"
    ;
> {
    pass Bleach < string Script = "Draw=Buffer;"; > {
        //AlphaBlendEnable = FALSE;
        VertexShader = compile vs_3_0 VS_Bleach();
        PixelShader  = compile ps_3_0 PS_Bleach();
    }

    pass Judge < string Script = "Draw=Buffer;"; > {
        //AlphaBlendEnable = FALSE;
        VertexShader = compile vs_3_0 VS_Judge();
        PixelShader  = compile ps_3_0 PS_Judge(); 
    }

    pass DrawBuffer < string Script = "Draw=Buffer;"; > {
        //AlphaBlendEnable = FALSE;
        VertexShader = compile vs_3_0 VS_BasicBuffer();
        PixelShader  = compile ps_3_0 PS_BasicBuffer();
    }


}
////////////////////////////////////////////////////////////////////////////////////////////////
