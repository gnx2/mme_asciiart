# MME_AsciiArt

## About
出力画像の明暗をアスキー文字で表現するエフェクト

出力サイズが  

 * 横：8の倍数  
 * 縦：16の倍数

でないと汚いです。

1280x720か1280x960あたりがいいと思います。
アンチエイリアスはOFFにしたほうが文字がはっきりでます。

編集画面ではまず綺麗に表示されません。

### 使い道
圧倒的な圧縮効率


## Fonts License
http://www.inp.nsk.su/~bolkhov/files/fonts/univga/  
Unicode VGA fontを使用しています。

↓サイトより規約引用
```
Copyright and acknowledgements

The UNI-VGA font can be distributed and modified freely, according to the X license.

The Basic Latin block was taken almost unchanged from DosEmu's vga.bdf. All the other blocks (except noted) were created mainly from scratch by me, Dmitry Bolkhovityanov.

Letters in the Hebrew block were taken unchanged from a public domain hebrew console font.

Glyphs in Arabic, Arabic Presentation Forms-A, Arabic Presentation Forms-B and U+262B Farsi symbol were kindly donated by Behdad Esfahbod.

Thanks to Birger Langkjer for idea, to Mark Leisher for his wonderful XmBDFEd, and to many others for their support.
```
引用ここまで


## Agreement
 * MITライセンス
 * 何があっても責任は持ちません。
 * 利用方法、利用場所について一切の制限をいたしません。


## Release
 * 2019/04/14
   * ソースコードのGitLabホストに伴うドキュメント整理

 * 2014/09/20
   * 修正
   * 静画アップロード

 * 2014/09/18
   * 公開


## Author
**GNX2**
